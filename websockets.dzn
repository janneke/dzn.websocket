// Copyright © 2017 Jan Nieuwenhuizen <janneke@gnu.org>

// This file is part of dzn.websocket

// dzn.websocket is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of the
// License, or (at your option) any later version.

// dzn.websocket is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with dzn.websocket.  If not, see
// <http://www.gnu.org/licenses/>.

import websocket-types.dzn;

interface iwebsocket {
  in void open (url_t url);
  in void connection (websocket_t ws);
  in void close ();
  in void free ();
  in void send (message_t msg);
  in void get (out websocket_t ws);

  out void connected (websocket_t ws);
  out void error (websocket_t ws, error_t e);
  out void message (websocket_t ws, message_t msg);
  out void disconnect (websocket_t ws, message_t msg);
  behaviour {
    enum state {closed,opening,open};
    state s=state.closed;
    [s.closed] {
      on open: s=state.opening;
      on connection: s=state.open;

      on get: illegal;
      on close: {}
      on free: {}
      on send: illegal;
    }
    [s.opening] {
      on inevitable: {s=state.open;connected;}
      on inevitable: {s=state.closed;error;}
      on close: s=state.closed;
      on free: s=state.closed;

      on get: illegal;
      on send: illegal;
      on open: illegal;
      on connection: illegal;
    }
    [s.open] {
      on get: {}
      on send: {}
      on send: {s=state.closed;error;}
      on optional: message;
      on close: {s=state.closed;disconnect;}
      on free: s=state.closed;
      on inevitable: {s=state.closed;error;}
      on inevitable: {s=state.closed;disconnect;}

      on open: illegal;
      on connection: illegal;
    }
  }
}

interface iwebsocket_server {
  in void open (config_t cfg);
  in void close (inout websocket_server_t wss);
  out void opened (websocket_server_t wss);
  out void connection (websocket_t ws);
  out void error (message_t msg);

  behaviour {
    enum state {closed,opening,open};
    state s=state.closed;
    [s.closed] {
      on open: s=state.opening;
      on open: error;
      on close: illegal;
      }
    [otherwise] {
      on open: illegal;
      on close: s=state.closed;
      on inevitable: {s=state.closed;error;}
      [s.opening] on inevitable: {s=state.open;opened;}
      [s.open] on inevitable: connection;
    }
  }
}

interface itimer {
  in void create (milliseconds_t ms);
  in void cancel ();
  out void timeout ();

  behaviour {
    enum state {idle,pending};
    state s = state.idle;
    [s.idle] {
      on create: s=state.pending;
      on cancel: {}
    }
    [s.pending] {
      on create: illegal;
      on cancel: s=state.idle;
      on inevitable: {timeout; s=state.idle;}
    }
  }
}

component timer {
  provides itimer port;
  // behaviour {on port.create (ms): {} on port.cancel (): {}}
}

interface iwebsocket_client {
  in void open (url_t url);
  in void cancel ();
  out void connected (websocket_t ws);
  out void error (error_t e);
  behaviour {
    bool opening=false;
    [!opening] {
      on open: opening=true;
      on open: error;
      on cancel: illegal;
    }
    [opening] {
      on open: error;
      on cancel: opening=false;
      on inevitable: {opening=false;connected;}
      on inevitable: {opening=false;error;}
    }
  }
}

component websocket_client {
  provides iwebsocket_client port;
  requires iwebsocket socket;
  requires itimer timer;
  behaviour {
    enum state {closed,opening,retrying};
    subint range {0..2};
    state s=state.closed;
    range retry=0;
    url_t url;
    object_t foo;
    on socket.message (ws,msg): {}
    on socket.disconnect (ws,msg): {}
    [s.closed] on port.open (u): {s=state.opening;retry=0;url=u;socket.open (url);}
    [!s.closed] on port.open (url): {error_t e;port.error (e);}
    [s.opening] {
      on port.cancel (): {s=state.closed;socket.free ();}
      [retry<2] on socket.error (ws,e): {s=state.retrying;timer.create ($1000$);socket.close ();}
      [retry==2] on socket.error (ws,e): {s=state.closed;retry=0;port.error (e);}
      on socket.connected (ws): {s=state.closed;retry=0;socket.free ();port.connected (ws);}
    }
    [s.retrying] {
      on port.cancel (): {s=state.closed;retry=0;timer.cancel ();}
      on timer.timeout (): {s=state.opening;retry=retry+1;socket.open (url);}
    }
  }
}

interface iwebsocket_pool {
  in void add (id_t id, websocket_t ws);
  in void remove (id_t id);

  in void send (id_t id, message_t msg);
  in void broadcast (message_t msg);
  in void close ();

  out void error (id_t id, error_t e);
  out void message (id_t id, message_t msg);
  out void disconnect (id_t id, message_t msg);
  out void closed ();

  behaviour {
    enum state {closed,open};
    state s = state.closed;
    on add: s=state.open;
    [s.closed] {
      on remove: illegal;
      on send: illegal;
      on broadcast: illegal;
      on close: illegal;
    }
    [s.open] {
      on close: s=state.closed;
      on send: {}
      on send: error;
      on send: {s=state.closed;error;closed;}
      on broadcast: {}
      on broadcast: {s=state.closed;closed;} // FIXME: no error[s!] on broadcast
      on remove: {}
      on remove: {s=state.closed;closed;}
      on optional: error;
      on inevitable: {s=state.closed;error;closed;}
      on inevitable: disconnect;
      on inevitable: {s=state.closed;disconnect;closed;}
      on optional: message;
    }
  }
}

interface ilist {
  in void add (object_t o);
  in void remove (object_t o);
  in bool empty ();
  in bool begin (out iterator_t it);
  in bool next (inout iterator_t it, out object_t o);
  behaviour {
    on add: {}
    on remove: {}
    on empty: reply (false);
    on empty: reply (true);
    on begin: reply (false);
    on begin: reply (true);
    on next: reply (false);
    on next: reply (true);
  }
}

interface iwebsocket_list {
  in void close (websocket_t ws);
  in void get (out websocket_t ws);
  in void send (websocket_t ws, message_t msg);
  out void closed ();
  out void connected (websocket_t ws);
  out void disconnect (websocket_t ws, message_t msg);
  out void error (websocket_t ws, error_t e);
  out void message (websocket_t ws, message_t msg);
  behaviour {
    enum state {closed,open};
    state s=state.closed;
    [s.closed] {
      on inevitable: {s=state.open;connected;}
      on send: illegal;
      on close: illegal;
      on get: illegal;
    }
    [s.open] {
      on inevitable: connected;
      on inevitable: disconnect;
      on inevitable: {s=state.closed;closed;}
      on close: {}
      on close: {s=state.closed;closed;}
      on get: {}
      on send: {}
      on send: error;
      on send: {s=state.closed;error;closed;}
      on optional: message;
    }
  }
}

component websocket_server {
  provides iwebsocket_server port;
  //behaviour {}
}

component websocket {
  provides iwebsocket port;
  //behaviour {}
}

component websocket_pool {
  provides iwebsocket_pool port;
  // behaviour {
  //   enum state {closed,open};
  //   state s = state.closed;
  //   map_t sockets = ${}$;

  //   void remove (id_t id) {
  //     sockets=$this.sockets[id].onclose=function(){};this.sockets[id].onerror=function(){};this.sockets[id].onmesage=function(){}; this.sockets[id].onopen=function(){};this.sockets;delete this.sockets[id];this.s=this.sockets=={}?this.state.closed:this.state.open$;
  //     if (s.closed) port.closed ();
  //   }
  //   on port.add (id, ws): {
  //     s=state.open;
  //     sockets=$this.sockets;this.sockets[id]=ws$;
  //   }
  //   [s.open] {
  //     on port.remove (id): {
  //       remove (id);
  //     }
  //     on port.send (id,msg): {
  //       websocket_t ws = $this.sockets[id];ws.send (JSON.stringify (msg),function (e){if (e){this.port.out.error(id,e);this.remove(id);}}.bind (this))$;
  //     }
  //     on port.broadcast (msg): {
  //       sockets = $this.sockets;Object.keys (this.sockets) (function (id){this.sockets[id].send (msg, function (e){if (e)this.remove (id);}.bind (this));}.bind (this));$;
  //     }
  //   }
  // }
}

component websocket_list {
  provides ilist list;
  provides iwebsocket_list websocket;
  //behaviour {}
}
