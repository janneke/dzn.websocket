// Copyright © 2017 Jan Nieuwenhuizen <janneke@gnu.org>

// This file is part of dzn.websocket

// dzn.websocket is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of the
// License, or (at your option) any later version.

// dzn.websocket is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with dzn.websocket.  If not, see
// <http://www.gnu.org/licenses/>.

dzn.websocket_pool = function (locator, meta) {
  this._dzn = {};
  this._dzn.locator = locator;
  this._dzn.rt = locator.get(new dzn.runtime());
  this._dzn.rt.components = (this._dzn.rt.components || []).concat ([this]);
  this._dzn.meta = meta;
  this._dzn.meta.ports = ['port'];
  this._dzn.meta.children = [];
  //this._dzn.flushes = true;

  this.state = {closed: 'state_closed',open: 'state_open'};

  this.s = this.state.closed;
  this.sockets = {};

  this.port = new dzn.iwebsocket_pool({provides: {name: 'port', component: this}, requires: {}});

  this.port.in.add = function(id,ws){
    this.s=this.state.open;
    this.sockets[id.id]=ws;
    ws.onerror=function(e){this.port.out.error (id,e);this.remove (id);}.bind (this);
    ws.onclose=function(e){this.port.out.disconnect (id);this.remove (id);}.bind (this);
    ws.onmessage=function (event) {
      try {var msg = JSON.parse (event.data);}
      catch (e) {process.nextTick();this.port.out.error (id,e);this.remove (id);}
      this.port.out.message (id, msg);
    }.bind (this);
  };
  this.port.in.remove = function(id){
    if (this.s==this.state.open) {
      this.remove (id);
    }
    else this._dzn.rt.illegal();
  };
  this.port.in.send = function(id,msg){
    if (this.s == this.state.open) {
      this.sockets[id.id].send (JSON.stringify (msg),function (e){if (e){this.port.out.error(id,e);this.remove (id);}}.bind (this));
    }
    else this._dzn.rt.illegal();
  };
  this.port.in.broadcast = function(msg){
    if (this.s == this.state.open) {
      Object.keys (this.sockets).map (function (id){this.sockets[id].send (JSON.stringify (msg), function (e){if (e)this.remove (id);}.bind (this));}.bind (this));
    }
    else this._dzn.rt.illegal();
  };

  this.port.in.close = function(msg){
    if (this.s == this.state.open) {
      Object.keys (this.sockets).map (function (id){this.sockets[id].onclose=null;this.sockets[id].close ();delete this.sockets[id];}.bind (this));
    }
    else this._dzn.rt.illegal();
  }

  this.remove = function (id) {
    var ws = this.sockets[id];
    if (ws) {
      ws.onclose=function(){};
      ws.onerror=function(){};
      ws.onmesage=function(){};
      ws.onopen=function(){};
    }
    delete this.sockets[id];
    this.s=this.sockets=={}?this.state.closed:this.state.open;
    if (this.s == this.state.closed) this.port.out.closed();
  }

  this._dzn.rt.bind(this);
};
