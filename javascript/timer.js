// Copyright © 2017 Jan Nieuwenhuizen <janneke@gnu.org>

// This file is part of dzn.websocket

// dzn.websocket is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of the
// License, or (at your option) any later version.

// dzn.websocket is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with dzn.websocket.  If not, see
// <http://www.gnu.org/licenses/>.

dzn.timer = function (locator, meta) {
  this._dzn = {};
  this._dzn.locator = locator;
  this._dzn.rt = locator.get (new dzn.runtime ());
  this._dzn.rt.components = (this._dzn.rt.components || []).concat ([this]);
  this._dzn.meta = meta;
  this._dzn.meta.ports = ['port'];
  this._dzn.meta.children = [];
  //this._dzn.flushes = true;

  this.port = new dzn.itimer ({provides: {name: 'port', component: this}, requires: {}});

  this.port.in.create = function (ms) {
    this.timer = setTimeout (this.port.out.timeout, ms);
  };

  this.port.in.cancel = function () {
    delete this.timer;
    this.timer = null;
  };

  this._dzn.rt.bind (this);
};
