// Copyright © 2017 Jan Nieuwenhuizen <janneke@gnu.org>

// This file is part of dzn.websocket

// dzn.websocket is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of the
// License, or (at your option) any later version.

// dzn.websocket is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with dzn.websocket.  If not, see
// <http://www.gnu.org/licenses/>.

dzn.websocket = function (locator, meta) {
  this._dzn = {};
  this._dzn.locator = locator;
  this._dzn.rt = locator.get (new dzn.runtime ());
  this._dzn.rt.components = (this._dzn.rt.components || []).concat ([this]);
  this._dzn.meta = meta;
  this._dzn.meta.ports = ['port'];
  this._dzn.meta.children = [];
  //this._dzn.flushes = true;

  this.port = new dzn.iwebsocket ({provides: {name: 'port', component: this}, requires: {}});

  this.id=10;
  this.set_id = function (o) {
    if (!o.id) o.id = ++this.id;
    return o.id;
  }.bind (this);

  this.close = function () {};
  this.get = function (out_ws) {};
  this.send = function (msg) {};

  this.connect = function (ws) {
    this.set_id (ws);
    ws.onerror = function (e) {this.port.out.error (ws, e);}.bind (this);
    ws.onclose = function (msg) {this.port.out.disconnect (ws, msg);delete ws;}.bind (this);
    ws.onmessage = function (event) {
      try {var msg = JSON.parse (event.data);}
      catch (e) {process.nextTick(function (){this.port.out.error (ws, e);}.bind (this));}
      this.port.out.message (ws, msg);
    }.bind (this);
    this.get = function (out_ws) {out_ws.value=ws;};
    this.close = function () {if (ws && ws.close) ws.close ();};
    this.free = function () {if (ws) ws.onclose = ws.onerror = ws.onmessage = ws.onopen = function () {};};
    this.send = function (msg) {ws.send (JSON.stringify (msg));};
  }.bind (this);

  this.port.in.open = function (url) {
    try {var ws = new require ('ws') (url);}
    catch (e) {process.nextTick(function () {this.port.out.error (ws, e);}.bind (this));}
    this.connect (ws);
    ws.onopen = function () {this.port.out.connected (ws);}.bind (this);
  };

  this.port.in.connection = this.connect;
  this.port.in.get = function (ws) {this.get (ws);};
  this.port.in.close = function () {this.close ();};
  this.port.in.free = function () {this.free ();};
  this.port.in.send = function (msg) {this.send (msg);};

  this._dzn.rt.bind (this);
};
