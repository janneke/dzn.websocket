// Copyright © 2017 Jan Nieuwenhuizen <janneke@gnu.org>

// This file is part of dzn.websocket

// dzn.websocket is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of the
// License, or (at your option) any later version.

// dzn.websocket is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with dzn.websocket.  If not, see
// <http://www.gnu.org/licenses/>.

dzn.websocket_list = function (locator, meta) {
  this._dzn = {};
  this._dzn.locator = locator;
  this._dzn.rt = locator.get(new dzn.runtime());
  this._dzn.rt.components = (this._dzn.rt.components || []).concat ([this]);
  this._dzn.meta = meta;
  this._dzn.meta.ports = ['list','websocket'];
  this._dzn.meta.children = [];
  //this._dzn.flushes = true;

  this.sockets = {};

  this.list = new dzn.ilist ({provides: {name: 'list', component: this}, requires: {}});
  this.websocket = new dzn.iwebsocket_list ({provides: {name: 'websocket', component: this}, requires: {}});

  this.remove = function (ws) {
    delete this.sockets[ws.id];
    if (this.sockets=={}) this.websocket.out.closed ();
  };

  this.list.in.add = function(ws){
    var w = new dzn.websocket (locator, {name: 'websocket'});
    w.port.in.connection (ws);
    w.port.out.message = this.websocket.out.message;
    w.port.out.error = this.websocket.out.error;
    w.port.out.disconnect = function (ws,msg){this.websocket.out.disconnect (ws,msg);delete this.sockets[ws.id];}.bind (this);
    this.sockets[ws.id] = w;
    if (Object.keys (this.sockets).length == 1) this.websocket.out.connected (ws);
  };
  this.list.in.remove = this.remove;
  this.list.in.empty = function () {return this.sockets == {};}
  this.list.in.begin = function (it) {
    it.value = Object.keys (this.sockets);
    return it.value.length;
  };
  this.list.in.next = function (it, ws) {
    var w = this.sockets[it.value[0]];
    w.port.in.get (ws);
    it.value = it.value.splice (1);
    return it.value.length;
  };
  this.websocket.in.send = function(ws,msg){
    this.sockets[ws.id].port.in.send (msg);};
  this.websocket.in.close = function(ws){
    this.sockets[ws.id].port.in.close ();
    this.remove (ws)
  };
  this._dzn.rt.bind(this);
};
