// Copyright © 2017 Jan Nieuwenhuizen <janneke@gnu.org>

// This file is part of dzn.websocket

// dzn.websocket is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of the
// License, or (at your option) any later version.

// dzn.websocket is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with dzn.websocket.  If not, see
// <http://www.gnu.org/licenses/>.

dzn.websocket_server = function (locator, meta) {
  this._dzn = {};
  this._dzn.locator = locator;
  this._dzn.rt = locator.get(new dzn.runtime());
  this._dzn.rt.components = (this._dzn.rt.components || []).concat ([this]);
  this._dzn.meta = meta;
  this._dzn.meta.ports = ['port'];
  this._dzn.meta.children = [];
  //this._dzn.flushes = true;

  this.port = new dzn.iwebsocket_server ({provides: {name: 'port', component: this}, requires: {}});

  this.id=10;
  this.set_id = function (o) {
    if (!o.id) o.id = ++this.id;
    return o.id;
  }.bind (this);

  this.port.in.open = function (cfg) {
    try {var wss = new (require ('ws').Server) (cfg.http ? {server:cfg.http} : {host:cfg.host,port:cfg.port});process.nextTick (function () {this.port.out.opened (wss);}.bind (this));}
    catch (e) {this.port.out.error (e);return;}
    if (cfg.http) console.log ('websocket_server listening: on http');
    else console.log ('websocket_server listening: %s:%j', cfg.host, cfg.port);
    wss.on ('connection', function (ws){this.set_id (ws);this.port.out.connection (ws);}.bind (this));
  };

  this.port.in.close = function (wss) {
    wss.close ();
    delete wss;
  };

  this._dzn.rt.bind (this);
};
