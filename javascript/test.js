#! /usr/bin/env node

// Copyright © 2017 Jan Nieuwenhuizen <janneke@gnu.org>

// This file is part of dzn.websocket

// dzn.websocket is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of the
// License, or (at your option) any later version.

// dzn.websocket is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with dzn.websocket.  If not, see
// <http://www.gnu.org/licenses/>.

function main () {
  var cfg = {host:'localhost',port:1234};
  var url = 'ws://'+cfg.host+':'+cfg.port;
  var dzn = require (__dirname + '/dzn/hello');
  var runtime = new dzn.runtime (function() {console.error ('test: illegal'); throw new Error ('test: oops: illegal'); });
  var loc = new dzn.locator ().set (runtime);
  var hello = new dzn.hello (loc, {name: 'hello'});
  hello.port.out.world = function (msg) {
    console.log ('response: %j', msg);
    hello.port.out.world = function (msg) {console.log ('goodbye: %j', msg)};
    //process.nextTick (function (){hello.port.in.hello (cfg,url,'hello');});
    setTimeout (function () {hello.port.in.hello  (cfg,url,'hello');}, 100);
  };
  hello.port.in.hello (cfg,url,'hello');
};

main ();
