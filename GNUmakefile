# Copyright © 2017 Jan Nieuwenhuizen <janneke@gnu.org>

# This file is part of dzn.websocket

# dzn.websocket is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.

# dzn.websocket is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with dzn.websocket.  If not, see
# <http://www.gnu.org/licenses/>.

include .config.make
-include .local.make

default: all

.config.make:
	./configure

SRC:=\
 javascript/websocket.js\
 javascript/websocket_pool.js\
 javascript/websocket_list.js\
 javascript/websocket_server.js\
 javascript/timer.js\
#

all: javascript/dzn/websockets.js javascript/dzn/hello.js javascript/dzn/runtime.js $(SRC:javascript/%=javascript/dzn/%)

clean:
	-rm -rf javascript/dzn

distclean: clean
	rm -f .config.make

javascript/dzn/%.js: %.dzn
	$(DZN) code -l javascript -I javascript -o javascript/ $^

javascript/dzn/runtime.js:
	$(DZN) cat /share/runtime/javascript/dzn/runtime.js > $@ || rm -f $@

javascript/dzn:
	mkdir -p $@

javascript/dzn/%.js: javascript/%.js | javascript/dzn
	ln -sf ../$(@F) $@

check: all
	$(NODE) javascript/test.js

verify:
	$(DZN) -v verify -l websockets.dzn
	$(DZN) -v verify -l hello.dzn

DATADIR:=$(DATADIR)$(PREFIX)/share/dzn.websocket
install: all
	mkdir -p $(DATADIR)/javascript
	cp -r websockets.dzn websocket-types.dzn hello.dzn hello-types.dzn javascript $(DATADIR)

